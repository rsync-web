# LaTeX2HTML 98.1p1 release (March 2nd, 1998)
# Associate images original text with physical files.


$key = q/{inline}alpha{inline}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.gif"
 ALT="$\alpha$">|; 

$key = q/{displaymath}a(k+1,l+1)=(a(k,l)-X_k+X_l+1)bmodM{displaymath}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="322" HEIGHT="28"
 SRC="img6.gif"
 ALT="\begin{displaymath}a(k+1,l+1) = (a(k,l) - X_k + X_{l+1}) \bmod M \end{displaymath}">|; 

$key = q/{displaymath}a(k,l)=(sum_i=k^lX_i)bmodM{displaymath}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="176" HEIGHT="56"
 SRC="img3.gif"
 ALT="\begin{displaymath}a(k,l) = (\sum_{i=k}^l X_i) \bmod M \end{displaymath}">|; 

$key = q/{displaymath}b(k,l)=(sum_i=k^l(l-i+1)X_i)bmodM{displaymath}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="240" HEIGHT="56"
 SRC="img4.gif"
 ALT="\begin{displaymath}b(k,l) = (\sum_{i=k}^l (l-i+1)X_i) \bmod M \end{displaymath}">|; 

$key = q/{inline}X_kldotsX_l{inline}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="img5.gif"
 ALT="$X_k \ldots X_l$">|; 

$key = q/{displaymath}b(k+1,l+1)=(b(k,l)-(l-k+1)X_k+a(k+1,l+1))bmodM{displaymath}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="454" HEIGHT="28"
 SRC="img7.gif"
 ALT="\begin{displaymath}b(k+1,l+1) = (b(k,l) - (l-k+1) X_k + a(k+1,l+1)) \bmod M \end{displaymath}">|; 

$key = q/{inline}beta{inline}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="img2.gif"
 ALT="$\beta$">|; 

1;

